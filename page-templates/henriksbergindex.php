<?php
/**
 * Template Name: Henriksberg Index
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="jumbotron jumbotron-fluid jumbotron-henriksberg">
  <div class="container">
    <h1 class="display-3"><?php the_field('jumbotron_heading'); ?></h1>
    <p class="lead"><?php the_field('jumbotron_subheading'); ?></p>
    <?php if( get_field('jumbotron_knapp') ): ?>
    	<hr class="my-4">
	  	<p class="lead">
	    	<a class="btn btn-primary btn-lg" href="<?php the_field('jumbotron_knapplank'); ?>" role="button"><?php the_field('jumbotron_knapptext'); ?></a>
	  	</p>
	
	 <?php endif; ?> 
  </div>
</div>

<?php get_template_part( 'global-templates/hero', 'none' ); ?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_html( $container ); ?>" id="content">

		<div class="row">

			<div
				class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-8<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">

				<main class="site-main" id="main" role="main">


					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :

							comments_template();

						endif;
						?>

					<?php endwhile; // end of the loop. ?>

					<?php wp_nav_menu(
					array(
						'theme_location'  => 'henriksberg-menu',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'nav justify-content-center',
						'fallback_cb'     => '',
						'menu_id'         => 'about-menu',
						'walker'          => new WP_Bootstrap_Navwalker(),
					)
				); ?>


				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar( 'right' ); ?>

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
